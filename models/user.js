'use strict';

const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Email address already in use!'
      },
      validate: {
        isEmail: true,
        isLowercase: true,
        notEmpty: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'This username is not available'
      },
      validate: {
        isLowercase: true,
        notEmpty: true
      }
    }
  }, {
    hooks: {
      beforeValidate: instance => {
        if (instance.email != null)
          instance.email = instance.email.toLowerCase();
      },

      beforeCreate: instance => {
        instance.encrypted_password = bcrypt.hashSync(instance.encrypted_password, 10);
      },
    }
  });

  User.associate = function (models) {
    User.hasOne(models.Profile, {
      foreignKey: `id_user`
    })

    User.hasMany(models.Review, {
      foreignKey: 'UserId'
    })
  };

  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
        name: this.name,
        username: this.username,
        access_token: this.getToken()
      }
    }
  })

  User.authenticate = async function ({
    email,
    password
  }) {
    try {
      let instance = await this.findOne({
        where: {
          email: email.toLowerCase()
        }
      })
      if (instance == null) return Promise.reject(new Error(`Email doesn't exist`))

      let isValidPassword = instance.checkCredential(password)
      if (!isValidPassword) return Promise.reject(new Error("Wrong password"))

      return Promise.resolve(instance)
    } catch (err) {
      return Promise.reject(new Error(err.message))
    }
  }

  User.prototype.checkCredential = function (password) {
    return bcrypt.compareSync(password, this.encrypted_password)
  }

  User.prototype.getToken = function () {
    return jwt.sign({
      id: this.id,
      email: this.email
    }, process.env.JWT_SECRET)
  }

  return User;
};