'use strict';
module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.Sequelize.Model

  class Movie extends Model {}

  Movie.init({
    title: DataTypes.STRING,
    trailer: DataTypes.TEXT,
    poster: {
      type: DataTypes.TEXT,
      validate: {
        isUrl: true
      }
    },
    director: DataTypes.STRING,
    released: {
      type: DataTypes.DATE,
      validate: {
        isDate: true
      }
    },
    actor: DataTypes.TEXT,
    languages: DataTypes.STRING,
    genre: DataTypes.STRING,
    synopsis: DataTypes.TEXT
  }, {
    sequelize
  })
  // const Movie = sequelize.define('Movie', {

  // }, {});
  Movie.associate = function (models) {
    // associations can be defined here
    Movie.hasMany(models.Review, {
      foreignKey: 'MovieId'
    })
  };
  return Movie;
};