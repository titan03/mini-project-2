require('dotenv').config();
const express = require('express');
const app = express()
const logger = require('morgan');
const session = require('express-session')
const cors = require('cors');

//! Core Module
const router = require('./routers')
const errorHandler = require('./middlewares/ErrorHandler')

//! set the template engine
app.set('view engine', 'ejs');

// ! for the session
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false
    }
}))

app.use(express.json())
app.use(express.urlencoded({
    extended: false
}))

app.use(logger('dev'))
app.use(cors())

// ! for the static file
app.use(express.static(__dirname + '/public'));


app.get('/', (req, res) => {
    res.render('index', {
        title: 'MovieHub',
        message: 'Your trusted movie database and review'
    })
})

app.use(router)

//! Apply Exception Handler
errorHandler.forEach(handler =>
    app.use(handler)
)

module.exports = app