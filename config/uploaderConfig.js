const multer = require('multer')
const path = require('path')

module.exports = {
  development: {
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(
          null,
          path.resolve(__dirname, '..', 'public', 'uploads')
          )
      },
      filename: (req, file, cb) => {
        const split = file.originalname.split('.')
        const extension = split[split.length - 1]
        const name = file.fieldname + '-' + Date.now() + '.' + extension

        file.url = process.env.BASE_URL + '/uploads/' + name

        cb(null, name)
      }
    })
  },
  test: {
    // storage: multer.diskStorage({
    //   destination: (req, file, cb) => {
    //     cb(
    //       null,
    //       path.resolve(__dirname, '..', 'public', 'uploads')
    //       )
    //   },
    //   filename: (req, file, cb) => {
    //     const split = file.originalname.split('.')
    //     const extension = split[split.length - 1]
    //     const name = 'TEST-'+ file.fieldname + '-' + Date.now() + '.' + extension

    //     file.url = process.env.BASE_URL + '/uploads/' + name

    //     cb(null, name)
    //   }
    // })
  },
  production: {}
}