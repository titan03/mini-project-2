const router = require('express').Router()
const Profile = require('../controllers/Profile')

const Success = require('../middlewares/SuccessHandler')
const Auth = require('../middlewares/Auth')
const Uploader = require('../middlewares/Uploader')

router.get('/', Auth, Profile.currentProfile, Success)
router.post('/uploadPhoto', Auth, Uploader.single('image'), Profile.uploadPhoto, Success)
router.get('/verify', Auth, Profile.sendVerificationToEmail, Success)
router.post('/verify', Auth, Profile.verifyFromEmail, Success)

module.exports = router