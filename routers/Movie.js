const router = require('express').Router();
const Movie = require('../controllers/Movie');
// const Auth = require('../middlewares/Auth');


router.get('/', Movie.getAll)
router.get('/:movieId', Movie.getRatingMovie)

module.exports = router