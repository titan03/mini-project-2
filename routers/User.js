const router = require('express').Router()
const User = require('../controllers/User')
const Success = require('../middlewares/SuccessHandler')
const Auth = require('../middlewares/Auth')


router.post('/login', User.login, Success) //! Login
router.get('/all', User.getAllUser, Success) //! Get All User
router.get('/', Auth, User.currentUser, Success) //! Get Current User
router.put('/', Auth, User.update, Success) //! Update Current User
router.delete('/', Auth, User.delete, Success) //! Delete Current User

module.exports = router