const router = require('express').Router()
const User = require('../controllers/User')
const Profile = require('../controllers/Profile')
const success = require('../middlewares/SuccessHandler')

router.post('/register', User.register, Profile.register, success) //! Register

module.exports = router