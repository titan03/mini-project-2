const {
    Review
} = require('../models');

module.exports = async (movieId) => {
    let getAverage = 0
    let totalRating = 0

    const rate = await Review.findAndCountAll({
        attributes: ['rating'],
        where: {
            MovieId: movieId
        }
    })

    if (rate.count > 0) {
        rate.rows.forEach(element => {
            if (element.rating === null) element.rating = 0
            getAverage = getAverage + element.rating
        });
        getAverage = getAverage / rate.count
        totalRating = getAverage.toFixed(1)
    }
    // console.log(rating);
    return parseInt(totalRating)
}