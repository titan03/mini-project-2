const {
  User
} = require('../models')
const bcrypt = require('bcryptjs')

module.exports = {
  register: async (req, res, next) => {
    try {
      let getBody = req.body
      getBody.encrypted_password = getBody.password
      const data = await User.create(getBody)
      res.data = data.entity
      res.status(201)
      next()
    } catch (error) {
      res.status(422)
      next(error)
    }
  },
  login: async (req, res, next) => {
    try {
      const data = await User.authenticate(req.body)
      res.status(200)
      res.data = data.entity
      next()
    } catch (error) {
      res.status(401)
      next(error)
    }
  },
  currentUser: async (req, res, next) => {
    try {
      res.data = await User.findOne({
        where: {
          id: req.user.id //req.user.id is id from token
        }
      })
      res.status(200)
      next()
    } catch (error) {
      res.status(401)
      next(error)
    }
  },
  getAllUser: async (req, res, next) => {
    try {
      res.data = await User.findAll()
      res.status(200)
      next()
    } catch (error) {
      res.status(401)
      next(error)
    }
  },
  update: async (req, res, next) => {

    try {
      //Get current user id from token
      let getId = req.user.id

      // If password is exist in body, hash first before updating to database
      if (req.body.password != null)
        req.body.encrypted_password = bcrypt.hashSync((req.body.password).toString(), 10)

      // Update data
      await User.update(req.body, {
        where: {
          id: getId
        }
      })

      // Show data after update
      const data = await User.findOne({
        where: {
          id: getId
        }
      })
      res.status(200)
      res.data = {
        message: `data from user id ${getId} is successfully updated`,
        data
      }
      next()
    } catch (error) {
      res.status(422)
      next(error)
    }
  },
  delete: async (req, res, next) => {
    try {
      //Get current user id from token
      let getId = req.user.id

      // Delete data
      await User.destroy({
        where: {
          id: getId
        }
      })
      res.status(200)
      res.data = `data from id ${getId} is deleted`
      next()
    } catch (error) {
      res.status(422)
      next(error)
    }
  }
}