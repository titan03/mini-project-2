const {
    Movie,
    Review,
    User
} = require('../models');
const {
    Op
} = require('sequelize');
const Rating = require('../helper/CountRating');
class Movies {
    static async getAll(req, res, next) {
        let movie = req.query.movie
        if (movie) {
            const movies = await Movie.findAndCountAll({
                where: {
                    [Op.or]: {
                        title: {
                            [Op.like]: `%${movie}%`
                        },
                        genre: {
                            [Op.like]: `%${movie}%`
                        }
                    }
                },
                limit: req.query.limit || 10,
                offset: (req.query.page - 1) * req.query.limit || 0
            }).catch((err) => {
                res.status(422);
                next(err)
            })

            if (movies) {
                res.status(200).json({
                    status: 'Success',
                    data: [movies]
                })
            }
        } else {
            const movies = await Movie.findAndCountAll({
                limit: req.query.limit || 10,
                offset: (req.query.page - 1) * req.query.limit || 0
            }).catch((err) => {
                res.status(422);
                next(err)
            })

            if (movies) {
                res.status(200).json({
                    status: 'Success',
                    data: [movies]
                })
            }
        }
    }

    static async getRatingMovie(req, res, next) {
        try {
            const movie = await Movie.findByPk(req.params.movieId)
            let averageRating = await Rating(req.params.movieId)
            const data = {
                movie,
                averageRating
            }
            res.status(200).json({
                status: 'Success',
                data: [data]
            })
        } catch (err) {
            res.status(422);
            next(err)
        }
    
    }
}

module.exports = Movies