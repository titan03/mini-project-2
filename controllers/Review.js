const {
    Review,
    User,
    Movie
} = require('../models');
const {
    Op
} = require('sequelize');

class Reviews {
    static async create(req, res, next) {
        try {
            const {
                description,
                rating,
                title
            } = req.body

            // const movie = await Movie.findByPk(req.params.id) //! first find the movie for create the review

            const moreReview = await Review.findOne({
                where: {
                    UserId: req.user.id,
                    MovieId: req.movies.id
                }
            })
            if (moreReview) {
                throw new Error('You can only have one review in each movies')
            }

            const review = await Review.create({
                description,
                rating,
                title,
                UserId: req.user.id,
                MovieId: req.movies.id
            })

            if (review) {
                res.status(201).json({
                    status: 'Success',
                    data: [review]
                })
            }

        } catch (error) {
            console.log(error.message);
            res.status(422);
            next(error)
        }
    }

    static async getAll(req, res, next) {
        let review = req.query.review
        if (review) {
            const reviews = await Review
                .findAndCountAll({
                    where: {
                        description: {
                            [Op.like]: `%${review}%`
                        }
                    },
                    limit: req.query.limit || 10,
                    offset: (req.query.page - 1) * req.query.limit || 0,
                    attributes: {
                        exclude: ['encrypted_password', 'createdAt', 'updatedAt']
                    },
                    include: [{
                        model: Movie
                    }, {
                        model: User
                    }],
                    order: [
                        ['id', 'ASC']
                    ]
                })
                .catch((err) => {
                    res.status(422)
                    next(err)
                })
            if (reviews) {
                res.status(200).json({
                    status: 'Success',
                    data: [reviews]
                })
            }
        } else {
            const reviews = await Review
                .findAndCountAll({
                    limit: req.query.limit || 10,
                    offset: (req.query.page - 1) * req.query.limit || 0,
                    attributes: {
                        exclude: ['encrypted_password', 'createdAt', 'updatedAt']
                    },
                    include: [{
                        model: Movie
                    }, {
                        model: User
                    }],
                    order: [
                        ['id', 'ASC']
                    ]
                })
                .catch((err) => {
                    res.status(422)
                    next(err)
                })
            if (reviews) {
                res.status(200).json({
                    status: 'Success',
                    data: [reviews]
                })
            }
        }
    }

    static async update(req, res, next) {
        const {
            description,
            rating,
            title
        } = req.body
        const id = req.params.id
        // await Movie.findOne({ //! first find the movie by id
        //     where: {
        //         id: req.query.id
        //     }
        // })
        const review = await Review.update({
            description,
            rating,
            title
        }, {
            where: {
                id: id
            }
        }).catch((err) => {
            // console.log(err.message);
            res.status(422)
            next(err)
        })

        if (review) {
            res.status(202).json({
                status: 'Success',
                message: `Review with id: ${id} has been updated`
            })
        }
    }

    static async delete(req, res, next) {
        const id = req.params.id
        // const movie = await Movie.findByPk(req.params.id) //! first find the movie by id
        const review = await Review.destroy({
            where: {
                id: id
            }
        }).catch((err) => {
            res.status(422)
            next(err)
        })

        if (review) {
            res.status(200).json({
                status: 'Success',
                message: `Review with id: ${id} has been deleted`
            })
        }
    }
}

module.exports = Reviews