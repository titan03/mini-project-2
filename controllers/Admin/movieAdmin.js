const {
    Movie
} = require('../../models');
const {
    Op
} = require("sequelize");

class Movies {

    static formCreate(req, res) {
        res.status(200).render('formCreate')
    }

    static create(req, res) {
        const {
            title,
            trailer,
            poster,
            director,
            released,
            actor,
            languages,
            genre,
            synopsis
        } = req.body

        Movie.create({
            title,
            trailer,
            poster,
            director,
            released,
            actor,
            languages,
            genre,
            synopsis
        }).then(() => {
            res.status(201).redirect('/movies')
        }).catch((err) => {
            res.status(400).render('error', {
                err
            })
        })

    }

    static getAll(req, res) {
        let movie = req.query.movie
        if (movie) {
            Movie.findAll({
                where: {
                    [Op.or]: {
                        title: {
                            [Op.like]: `%${movie}%`
                        },
                        genre: {
                            [Op.like]: `%${movie}%`
                        }
                    }
                },
                order: [
                    ['id', 'ASC']
                ]
            }).then((result) => {
                let user = req.session.user
                res.status(200).render('movies', {
                    result,
                    user
                })
            }).catch((err) => {
                res.status(400).render('error', {
                    err
                })
            })
        } else {
            Movie.findAll({
                order: [
                    ['id', 'ASC']
                ]
            }).then((result) => {
                let user = req.session.user
                // console.log(user);
                res.status(200).render('movies', {
                    result,
                    user
                })
            }).catch((err) => {
                res.status(400).render('error', {
                    err
                })
            })
        }


    }

    static async editForm(req, res) {
        const id = req.params.id
        const movie = await Movie.findByPk(id).catch((err) => {
            res.status(400).render('error', {
                err
            })
        })

        if (movie) {
            res.status(200).render('formEdit', {
                movie
            })
        }
    }

    static async editMovie(req, res) {
        const id = req.params.id
        const {
            title,
            trailer,
            poster,
            director,
            released,
            actor,
            languages,
            genre,
            synopsis
        } = req.body
        const movie = await Movie.update({
            title,
            trailer,
            poster,
            director,
            released,
            actor,
            languages,
            genre,
            synopsis
        }, {
            where: {
                id: id
            }
        }).catch((err) => {
            res.status(400).render('error', {
                err
            })
        })

        if (movie) {
            res.status(202).redirect('/movies')
        }
    }

    static async delete(req, res) {
        let id = req.params.id
        const movie = await Movie.destroy({
            where: {
                id
            }
        }).catch((err) => {
            res.status(400).render('error', {
                err
            })
        })

        if (movie) {
            res.status(200).redirect('/movies')
        }
    }
}

module.exports = Movies