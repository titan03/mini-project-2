const {
    User
} = require('../../models');
const bcrypt = require('bcryptjs');

class Admins {
    static loginAdmin(req, res) {
        const {
            email,
            password
        } = req.body
        User.findOne({
                where: {
                    email: email.toLowerCase()
                }
            }).then((user) => {
                if (user && bcrypt.compareSync(password, user.encrypted_password)) {
                    req.session.user = {
                        email: user.email,
                    }
                    res.status(201).redirect('/movies')
                } else {
                    res.status(400).redirect(`/login?err=${err.message}`)
                }
            })
            .catch(err => {
                console.log(err.message);
                res.status(400).redirect(`/login?err=${err.message}`)
            })

    }
}

module.exports = Admins