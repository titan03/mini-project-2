const jwt = require('jsonwebtoken')
const imagekit = require('../lib/imagekit')
const {
  Profile
} = require('../models')

module.exports = {
  register: async (req, res, next) => {
    try {
      req.body.admin = false // This to ensure that admin always false in creation
      req.body.id_user = res.data.id // Get user id from previous res value
      const newProfile = await Profile.create(req.body)
      res.data.profile = newProfile
      res.status(201)
      next()
    } catch (error) {
      res.status(422)
      next(error)
    }
  },
  currentProfile: async (req, res, next) => {
    try {
      res.data = await Profile.findOne({
        where: {
          id_user: req.user.id
        }
      })
      res.status(200)
      next()
    } catch (error) {
      next(error)
    }
  },
  uploadPhoto: async (req, res, next) => {
    console.log(req.file)
    try {
      const file = await imagekit.upload({
        file: req.file.buffer,
        fileName: req.file.originalname
      })
      res.data = 'Your photo profile is updated'
      await Profile.update({
        image_url: file.url
      }, {
        where: {
          id_user: req.user.id
        }
      })
      next()
    } catch (error) {
      next(error)
    }
  },
  sendVerificationToEmail: async (req, res, next) => {
    const sgMail = require('@sendgrid/mail');

    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    try {
      console.log(req.user.email)
      const tokenVerify = jwt.sign({
        id: req.user.id,
        email: req.user.email
      }, process.env.JWT_VERIFY)
      const url = process.env.BASE_URL + '/api/v1/profiles/verify?token=' + tokenVerify
      const msg = {
        to: req.user.email,
        from: 'rasyidproject@gmail.com',
        subject: 'Verification for your account in Moviepedia',
        text: 'This is verification link : ' + url,
        html: '<strong>This is verification link : ' + url + '</strong>',
      };
      await sgMail.send(msg)
      res.data = `Verification link has been sent to your email, please check the inbox. If there isn't, you can try to check the spam`
      next()
    } catch (error) {
      console.log(error)
      next(error)

      if (error.response) {
        console.error(error.response.body)
      }
    }
  },
  verifyFromEmail: async (req, res, next) => {
    try {
      let payload = jwt.verify(req.query.token, process.env.JWT_VERIFY)
      await Profile.update({
        verified: true
      }, {
        where: {
          id_user: req.user.id
        }
      })
      res.data = `Your profile is now verified`
      next()
    } catch (error) {
      next(error)
    }
  }
}