'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Movies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      trailer: {
        type: Sequelize.TEXT
      },
      poster: {
        type: Sequelize.TEXT
      },
      director: {
        type: Sequelize.STRING
      },
      released: {
        type: Sequelize.DATE
      },
      actor: {
        type: Sequelize.TEXT
      },
      languages: {
        type: Sequelize.STRING
      },
      genre: {
        type: Sequelize.STRING
      },
      synopsis: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Movies');
  }
};