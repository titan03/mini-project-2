'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Profiles', // table name
      'verified', // new field name
      {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Profiles','verified')
  }
};
