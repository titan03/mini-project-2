const request = require('supertest');
const db = require('../../models');
const app = require('../../app');

describe('Movie API Collection', () => {
    beforeAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Movies", "Profiles", "Reviews" RESTART IDENTITY');
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Movies", "Profiles", "Reviews" RESTART IDENTITY');
    })

    let access_token;
    // console.log(access_token);
    describe('POST /api/v1/register', () => {
        test('Status code 201 should successfully create new user', (done) => {
            request(app)
                .post('/api/v1/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'test1',
                    username: 'test1',
                    email: 'test1@mail.com',
                    password: '123456'
                })
                .then((res) => {
                    access_token = res.body.data.access_token
                    // console.log(access_token, '===================ini token test===================');
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success');
                    expect(res.body.data).toHaveProperty('access_token')
                    done()
                }).catch(console.log);
        });

        test('Status code 422 should can`t create new user', (done) => {
            request(app)
                .post('/api/v1/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'test1',
                    email: 'testmail.com',
                    password: '123456'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('fail');
                    expect(res.body).toHaveProperty('errors')
                    done()
                }).catch(console.log);
        });
    });

    // describe('POST /api/v1/reviews/:id', () => {
    //     test('Status code 201 should successfully create new review', (done) => {
    //         request(app)
    //             .post(`/api/v1/reviews/11`)
    //             .set('Content-Type', 'application/json')
    //             .set('Authorization', access_token)
    //             .send({
    //                 description: 'Good Movie',
    //                 rating: 8
    //             })
    //             .then((res) => {
    //                 expect(res.statusCode).toBe(201)
    //                 expect(res.body.status).toEqual('Success');
    //                 expect(res.body).toHaveProperty('data')
    //                 // MovieId = res.body.MovieId
    //                 // UserId = res.body.data.UserId
    //                 done()
    //             }).catch(console.log);
    //     });
    // })

    describe('GET /api/v1/reviews/all', () => {
        test('Status code 200 should successfully get all reviews', (done) => {
            request(app)
                .get('/api/v1/reviews/all')
                .set('Content-Type', 'application/json')
                .set('Authorization', access_token)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('Success');
                    expect(res.body).toHaveProperty('data')
                    done()
                }).catch(console.log);
        });
    });
});