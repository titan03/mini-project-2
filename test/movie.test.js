const request = require('supertest');
const db = require('../../models');
const app = require('../../app');


describe('Movie API Collection', () => {
    beforeAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Movies", "Profiles", "Reviews" RESTART IDENTITY');
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Movies", "Profiles", "Reviews" RESTART IDENTITY');
    })

    // let access_token;
    // console.log(access_token);
    // describe('POST /api/v1/register', () => {
    //     test('Status code 201 should successfully create new user', (done) => {
    //         request(app)
    //             .post('/api/v1/register')
    //             .set('Content-Type', 'application/json')
    //             .send({
    //                 name: 'test1',
    //                 email: 'test1@mail.com',
    //                 password: '123456'
    //             })
    //             .then((res) => {
    //                 access_token = res.body.data.access_token
    //                 console.log(access_token, '===================ini token test===================');
    //                 expect(res.statusCode).toBe(201)
    //                 expect(res.body.status).toEqual('success');
    //                 expect(res.body.data).toHaveProperty('access_token')
    //                 done()
    //             }).catch(console.log);
    //     });

    //     test('Status code 422 should can`t create new user', (done) => {
    //         request(app)
    //             .post('/api/v1/register')
    //             .set('Content-Type', 'application/json')
    //             .send({
    //                 name: 'test1',
    //                 email: 'testmail.com',
    //                 password: '123456'
    //             })
    //             .then((res) => {
    //                 expect(res.statusCode).toBe(422)
    //                 expect(res.body.status).toEqual('fail');
    //                 done()
    //             }).catch(console.log);
    //     });
    // });

    describe('GET api/v1/movies/', () => {
        test('Status code 200 should successfully get all movies', (done) => {
            // .set('Authorization', access_token)
            request(app)
                .get('/api/v1/movies')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('data')
                    done()
                }).catch(console.log);
        });
    });

    describe('GET api/v1/movies/:movieId', () => {
        test('Status code 200 should successfully get one movies and average rating', (done) => {
            // .set('Authorization', access_token)
            request(app)
                .get('/api/v1/movies')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('Success')
                    expect(res.body).toHaveProperty('data')
                    done()
                }).catch(console.log);
        });
    });
});