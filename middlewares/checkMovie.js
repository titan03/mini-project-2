const models = require('../models');

module.exports = modelsName => {
    const model = models[modelsName];
    return async function (req, res, next) {
        try {
            let movies = await model.findByPk(req.params.id);
            req.movies = movies;
            next();
        } catch (err) {
            res.status(400);
            next(err)
        }
    }
}