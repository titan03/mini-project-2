const models = require('../models');


module.exports = (modelName) => {
    const Model = models[modelName]

    return async (req, res, next) => {
        let id = req.params.id
        let instance = await Model.findByPk(id)
        if (instance.UserId === req.user.id) {
            next()
        } else {
            res.status(403).json({
                status: 'Failed',
                message: `You are not the owner`
            })
        }
    }
}