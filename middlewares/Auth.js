const jwt = require('jsonwebtoken');
const {
  User
} = require('../models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    let payload = jwt.verify(token, process.env.JWT_SECRET);
    req.user = await User.findByPk(payload.id);
    next();
  } catch {
    res.status(401);
    next(new Error("Invalid Token"));
  }
}