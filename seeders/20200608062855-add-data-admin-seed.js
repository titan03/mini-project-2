'use strict';
const bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
        username: 'admin1',
        name: 'Titan',
        email: 'admin1@mail.com',
        encrypted_password: bcrypt.hashSync('admin', bcrypt.genSaltSync(10)),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: 'admin2', 
        name: 'Rasyid',
        email: 'admin2@mail.com',
        encrypted_password: bcrypt.hashSync('admin', bcrypt.genSaltSync(10)),
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ], {});
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  }
};